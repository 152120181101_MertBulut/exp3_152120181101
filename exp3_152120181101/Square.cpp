/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
}

Square::~Square() {
}

void Square::setA(double a){
	this->a = a; 
}

void Square::setB(double a) {
	setA(a);
}

double Square::calculateCircumference(){
	return a * 4;
}

double Square::calculateArea(){
	return a * a;
}
